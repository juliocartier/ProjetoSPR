ARG RUBY_VERSION=2.7.4
FROM ruby:$RUBY_VERSION

RUN apt-get update -qq && \
    apt-get install -y build-essential libvips libpq-dev postgresql-client nodejs imagemagick sudo libxss1 unzip memcached cmake libsqlite3-dev sqlite3 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man

# Rails app lives here
WORKDIR /app

COPY Gemfile* .

RUN bundle install

RUN mkdir /db
RUN /usr/bin/sqlite3 /db/development.sqlite3

COPY . .

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]



## ESSA PARTE NAO É NECESSARIA FOI CRIADO COM A IMAGEM RUBY 2.7.4

# Install libvips for Active Storage preview support
# RUN apt-get update -qq && \
#     apt-get install -y build-essential libvips && \
#     apt-get clean && \
#     rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man

# RUN apt-get install -y build-essential libpq-dev postgresql-client nodejs imagemagick sudo libxss1 libappindicator1 libindicator7 unzip memcached cmake

# Set production environment
# ENV RAILS_LOG_TO_STDOUT="1" \
#     RAILS_SERVE_STATIC_FILES="true" \
#     RAILS_ENV="production" \
#     BUNDLE_WITHOUT="development"

# # Install application gems
# COPY Gemfile Gemfile.lock ./
# # RUN bundle install
# RUN gem install rails bundler

# # Copy application code
# COPY . .

# # Precompile bootsnap code for faster boot times
# RUN bundle exec bootsnap precompile --gemfile app/ lib/

# Precompiling assets for production without requiring secret RAILS_MASTER_KEY
# RUN SECRET_KEY_BASE_DUMMY=1 bundle exec rails assets:precompile

# Entrypoint prepares the database.
# ENTRYPOINT ["/rails/bin/docker-entrypoint"]

# Start the server by default, this can be overwritten at runtime
# EXPOSE 3000
# CMD ["./bin/rails", "server"]