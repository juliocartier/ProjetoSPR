class LocalizedController < ApplicationController
  def index
		respond_to do |format|
		  format.html { grafico }
		  format.js { render :json => result }
    end
  end

  def grafico

		@fusioncharts = Fusioncharts::Chart.new({
			  :type => 'zoomscatter',
			  :id => 'chartobject-1',
			  :renderAt => 'chart-container',
			  :width => '600',
			  :height => '350',
			  :dataFormat => 'json',
			  :dataSource => {
				:chart => {
				    #Theme
				    :theme => "fint",
				    :caption => 'Curve LSPR',
				    :subcaption => "WIM",
				    :yaxisname => 'Qext - Au NP\'s',
				    :xaxisname => 'Wavelenght (nm)',
				    :xaxismaxvalue => "1100",
				    :xaxisminvalue => "400",
				    :yaxismaxvalue => "1.1",
				    :yAxisMinValue => "0",
				    :xnumberprefix => "",
				    :ynumbersuffix => "",
				    :showXAxisLine => "3",
				    :showformbtn => "1",
				    :formAction => "#",
				    :exportEnabled => "1",
				    :submitdataasxml =>  "1"
				},
				:categories => [{
				    :verticallinecolor => "666666",
				    :verticallinethickness => "1",
				    :alpha => "20",
				    :category => []
				}],
				:dataset => [{
					:seriesname => "r",
				    :drawline => "1",
				    :color => "#3052DC",
				    :anchorsides => "0",
				    :anchorradius => "0",
				    :anchorbgcolor => "#3052DC",
				    :anchorbordercolor => "#3052DC",
				    :data => @valor

				},{
					:seriesname => "t",
					:drawline => "1",
				    :color => "#3052DC",
				    :anchorsides => "0",
				    :anchorradius => "0",
				    :anchorbgcolor => "#ef1414",
				    :anchorbordercolor => "#ef1414",
				    :data => @valor_t
				}]
			  }

		    })
			  @fusioncharts.render()

	end
end
