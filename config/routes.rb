Rails.application.routes.draw do
  #get 'modo_wim/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
#get 'welcome/matriz'
 # post 'welcome/matriz'

  #get 'welcome/menu'
  #post 'projeto/index'
  #get 'projeto/index'



  #resources :projeto
#  post 'projeto/fusioncharts.zoomscatter.js'
#  get 'projeto/fusioncharts.zoomscatter.js'

	#Rotas tipo GET do Modelo AIM

	get '/', to: 'projeto#indexCamada3'
	post '/', to: 'projeto#indexCamada3'
	#root  to: "projeto#indexCamada3"

	get '/index', to: 'localized#index'

	post '/index', to: 'localized#index'

	get '/indexCamada3', to: 'projeto#indexCamada3'
	get '/indexCamada4', to: 'projeto#indexCamada4'
	get '/indexCamada5', to: 'projeto#indexCamada5'
	get '/indexCamada6', to: 'projeto#indexCamada6'
	get '/indexCamada7', to: 'projeto#indexCamada7'
	get '/indexCamada8', to: 'projeto#indexCamada8'
	get '/indexCamada9', to: 'projeto#indexCamada9'
	get '/indexCamada10', to: 'projeto#indexCamada10'
	#get '/', to: 'projeto#indexCamada4'

	#root to: "projeto#indexCamada3"

	post '/indexCamada3', to: 'projeto#indexCamada3'
	post '/indexCamada4', to: 'projeto#indexCamada4'
	post '/indexCamada5', to: 'projeto#indexCamada5'
	post '/indexCamada6', to: 'projeto#indexCamada6'
	post '/indexCamada7', to: 'projeto#indexCamada7'
	post '/indexCamada8', to: 'projeto#indexCamada8'
	post '/indexCamada9', to: 'projeto#indexCamada9'
	post '/indexCamada10', to: 'projeto#indexCamada10'

	#Rotas tipo GET do Modelo WIM
	get '/indexCamada3Wim', to: 'modo_wim#indexCamada3Wim'
	get '/indexCamada4Wim', to: 'modo_wim#indexCamada4Wim'
	get '/indexCamada5Wim', to: 'modo_wim#indexCamada5Wim'
	get '/indexCamada6Wim', to: 'modo_wim#indexCamada6Wim'
	get '/indexCamada7Wim', to: 'modo_wim#indexCamada7Wim'
	get '/indexCamada8Wim', to: 'modo_wim#indexCamada8Wim'
	get '/indexCamada9Wim', to: 'modo_wim#indexCamada9Wim'
	get '/indexCamada10Wim', to: 'modo_wim#indexCamada10Wim'

	#Rotas tipo POST do Modelo WIM
	post '/indexCamada3Wim', to: 'modo_wim#indexCamada3Wim'
	post '/indexCamada4Wim', to: 'modo_wim#indexCamada4Wim'
	post '/indexCamada5Wim', to: 'modo_wim#indexCamada5Wim'
	post '/indexCamada6Wim', to: 'modo_wim#indexCamada6Wim'
	post '/indexCamada7Wim', to: 'modo_wim#indexCamada7Wim'
	post '/indexCamada8Wim', to: 'modo_wim#indexCamada8Wim'
	post '/indexCamada9Wim', to: 'modo_wim#indexCamada9Wim'
	post '/indexCamada10Wim', to: 'modo_wim#indexCamada10Wim'

	#Rotas tipo GET do sensograma
	get '/simulaCamada3', to: 'sensograma#simulaCamada3'
	get '/simulaCamada4', to: 'sensograma#simulaCamada4'
	get '/simulaCamada5', to: 'sensograma#simulaCamada5'
	get '/simulaCamada6', to: 'sensograma#simulaCamada6'
	get '/simulaCamada7', to: 'sensograma#simulaCamada7'
	get '/simulaCamada8', to: 'sensograma#simulaCamada8'
	get '/simulaCamada9', to: 'sensograma#simulaCamada9'


	#Rotas tipo POST do sensograma
	post '/simulaCamada3', to: 'sensograma#simulaCamada3'
	post '/simulaCamada4', to: 'sensograma#simulaCamada4'
	post '/simulaCamada5', to: 'sensograma#simulaCamada5'
	post '/simulaCamada6', to: 'sensograma#simulaCamada6'
	post '/simulaCamada7', to: 'sensograma#simulaCamada7'
	post '/simulaCamada8', to: 'sensograma#simulaCamada8'
	post '/simulaCamada9', to: 'sensograma#simulaCamada9'


	#get 'modo_wim/indexCamada3Wim'
	#post 'modo_wim/indexCamada3Wim'

	#post 'projeto/indexCamada3'
	#post 'projeto/indexCamada4'
	#post 'projeto/indexCamada5'
	#post 'projeto/indexCamada6'
	#post 'projeto/indexCamada7'
	#post 'projeto/indexCamada8'
	#post 'projeto/indexCamada9'
	#post 'projeto/indexCamada10'
	#post 'projeto/simula'



  #get 'projeto/simula'
  #get 'projeto/indexCamada3'
  #get 'projeto/indexCamada4'
  #get 'projeto/indexCamada5'
  #get 'projeto/indexCamada6'
  #get 'projeto/indexCamada7'
  #get 'projeto/indexCamada8'
  #get 'projeto/indexCamada9'
  #get 'projeto/indexCamada10'

 # post 'win/win'
 # get 'win/win'

 # post 'win/indexCamada4'
 # get 'win/indexCamada4'

 # post 'win/indexCamada5'
 # get 'win/indexCamada5'

 # post 'win/indexCamada5'
 # get 'win/indexCamada5'

 # post 'win/indexCamada6'
 # get 'win/indexCamada6'

 # post 'win/indexCamada7'
 # get 'win/indexCamada7'

 # post 'win/indexCamada8'
 # get 'win/indexCamada8'

 # post 'win/indexCamada9'
 # get 'win/indexCamada9'

 # post 'win/indexCamada10'
 # get 'win/indexCamada10'

end
