# -*- encoding: utf-8 -*-
# stub: spliner 1.0.6 ruby lib

Gem::Specification.new do |s|
  s.name = "spliner".freeze
  s.version = "1.0.6"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Tallak Tveide".freeze]
  s.date = "2013-06-19"
  s.description = "Simple library to perform cubic spline interpolation based on key X,Y values".freeze
  s.email = ["tallak@tveide.net".freeze]
  s.homepage = "http://www.github.com/tallakt/spliner".freeze
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.1".freeze)
  s.rubygems_version = "3.0.1".freeze
  s.summary = "Cubic spline interpolation library".freeze

  s.installed_by_version = "3.0.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>.freeze, ["~> 2.11"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 0.9"])
    else
      s.add_dependency(%q<rspec>.freeze, ["~> 2.11"])
      s.add_dependency(%q<rake>.freeze, ["~> 0.9"])
    end
  else
    s.add_dependency(%q<rspec>.freeze, ["~> 2.11"])
    s.add_dependency(%q<rake>.freeze, ["~> 0.9"])
  end
end
